package ru.kev.fractionsn;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класса Fraction.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */

class Fraction {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Fraction() {
        this.num = 1;
        this.denum = 1;
    }

    public void Split(String stroka) {
        String[] mass = new String[2];
        int i = 0;
        for (String retval : stroka.split("/")) {
            mass[i] = retval;
            i++;
        }
        this.num = Integer.parseInt(mass[0]);
        this.denum = Integer.parseInt(mass[1]);
    }

    public Fraction addition (Fraction fraction){
        Fraction fraction2 = new Fraction();
        if (fraction.denum==this.denum){
            fraction2.num=this.num+fraction.num;
            fraction2.denum=fraction.denum;
        }else {
            fraction2.num = this.num * fraction.denum + fraction.num * this.denum;
            fraction2.denum = this.denum * fraction.denum;
        }
        return fraction2;
    }
    public Fraction subtraction (Fraction fraction){
        Fraction fraction2 = new Fraction();
        if (fraction.denum==this.denum){
            fraction2.num=this.num-fraction.num;
            fraction2.denum=fraction.denum;
        }else {
            fraction2.num = this.num * fraction.denum - fraction.num * this.denum;
            fraction2.denum = this.denum * fraction.denum;
        }
        return fraction2;
    }
    public Fraction multiplication (Fraction fraction){
        Fraction fraction2 = new Fraction();
        fraction2.num=this.num*fraction.num;
        fraction2.denum=this.denum*fraction.denum;
        return fraction2;
    }
    public Fraction division (Fraction fraction){
        Fraction fraction2 = new Fraction();
        fraction2.num=this.num*fraction.denum;
        fraction2.denum=this.denum*fraction.num;
        return fraction2;
    }
    public boolean Error(String stroka) {
        Pattern pattern = Pattern.compile("^-?[1-9][0-9]*/-?[1-9][0-9]*\\s[+:*-]\\s-?[1-9][0-9]*/-?[1-9][0-9]*");
        Matcher matcher = pattern.matcher(stroka);
        return matcher.find();
    }

    @Override
    public String toString() {
        return num + "/" + denum;
    }


}
