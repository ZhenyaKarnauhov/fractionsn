package ru.kev.fractionsn;

import java.io.*;
import java.util.*;
import java.util.regex.*;


/**
 * Класса Demo.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */

public class Demo {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] arg) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\kev\\fractionsn\\file12.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\kev\\fractionsn\\file123.txt"));
        BufferedWriter error = new BufferedWriter(new FileWriter("src\\ru\\kev\\fractionsn\\error.txt"));

        Fraction fraction = new Fraction();
        Fraction fraction2 = new Fraction();
        String stroka;

        while ((stroka = bufferedReader.readLine()) != null) {
            if (!isCheck(stroka)){
                error.write(stroka);
                error.write("\r\n");
                continue;
            }
            String[] strings = stroka.split(" ");

            fraction.Split(strings[0]);
            fraction2.Split(strings[2]);
            switch (strings[1]) {
                case "+":
                    System.out.println(fraction.addition(fraction2));
                    bufferedWriter.write(String.valueOf(fraction.addition(fraction2))+"\n");
                    break;
                case "-":
                    System.out.println(fraction.subtraction(fraction2));
                    bufferedWriter.write(String.valueOf(fraction.subtraction(fraction2))+"\n");
                    break;
                case ":":
                    System.out.println(fraction.division(fraction2));
                    bufferedWriter.write(String.valueOf(fraction.division(fraction2))+"\n");
                    break;
                case "*":
                    System.out.println(fraction.multiplication(fraction2));
                    bufferedWriter.write(String.valueOf(fraction.multiplication(fraction2))+"\n");
                    break;
            }
        }

        bufferedReader.close();
        bufferedWriter.close();
        error.close();
    }
    public static boolean isCheck(String stroka) {
        Pattern pattern = Pattern.compile("^-?[1-9][0-9]*/-?[1-9][0-9]*\\s[+:*-]\\s-?[1-9][0-9]*/-?[1-9][0-9]*$");
        Matcher matcher = pattern.matcher(stroka);
        return matcher.find();
    }
}